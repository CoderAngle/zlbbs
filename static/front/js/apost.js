$(function () {
    var ue = UE.getEditor("editor",{
        'serverUrl':'/ueditor/upload/',
    });

    $("#submit-btn").click(function (event) {
        event.preventDefault();
        var titleInput = $('input[name="title"]');
        var boardSelect = $("select[name='board_id']");

        var title = titleInput.val();
        var board_id = boardSelect.val();
        // console.log(board_id)
        var content = ue.getContent();

        if( !title || ! board_id || !content){
            zlalert.alertInfo("请输入相应内容！");
            return ;
        }

        zlajax.post({
            'url':'/apost/',
            'data':{
                'title':title,
                'content':content,
                'board_id':board_id,
            },
            'success':function (data) {
                if(data['code'] == 200){
                    zlalert.alertConfirm({
                        'msg':"帖子发表成功",
                        'cancelText':'回到首页',
                        'confirmText':'再发一篇帖子',
                        'cancelCallback':function () {
                            window.location = '/';
                        },
                        'confirmCallback':function () {
                            titleInput.val("");
                            ue.setContent("");
                        }
                    });
                }else{
                    zlalert.alertNetworkError();
                }
            }
        });
    });
});