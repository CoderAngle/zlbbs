$(function () {
    var ue = UE.getEditor("editor",{
        'serverUrl':'/ueditor/upload/',
        toolbars: [[
        'undo', //撤销
        'redo', //重做
        'bold', //加粗
        'italic', //斜体
        'blockquote', //引用
        'selectall', //全选
        'insertcode', //代码语言
        'fontfamily', //字体
        'fontsize', //字号
        'paragraph', //段落格式
        'simpleupload', //单图上传
        'emotion', //表情
        'searchreplace', //查询替换
        'insertvideo', //视频
        'justifyleft', //居左对齐
        'justifyright', //居右对齐
        'justifycenter', //居中对齐
        'justifyjustify', //两端对齐
        'forecolor', //字体颜色
    ]],
    });
    window.ue = ue;
});

$(function () {
    $('#comment-btn').click(function (event) {
        event.preventDefault();

        var loginTag = $('#login-tag').attr("data-is-login");
        // console.log(loginTag);
        if (!loginTag) {
            window.location = '/signin/';
        }else{
            var content = window.ue.getContent();
            var post_id = $("#post-content").attr('data-id');

            zlajax.post({
                'url':'/acomment/',
                'data':{
                  'content':content,
                  'post_id':post_id,
                },
                'success':function (data) {
                    if(data['code'] == 200) {
                        window.location.reload();
                    }
                    else{
                        zlalert.alertInfo(data["message"]);
                    }
                },
                'fail':function () {
                    zlalert.alertNetworkError();
                }
            });
        }
    });
});


$(function () {
    $('.like-star').click(function (event) {
        event.preventDefault();

        var self = $(this);
        var post_id = self.attr("data-id");
        var like_text = $('.like_text').text();
        zlajax.get({
           'url':'/alike/',
            'data':{
                "post_id":post_id,
            },
            'success':function () {
               window.location.reload();
            }
        });
    });
});

