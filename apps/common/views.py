from flask import Blueprint,request,make_response,jsonify
from utils.sms import demo_sms_send
from utils import restful,zlcache
from utils.captcha import Captcha
from .forms import SMSCaptchaForm
from io import BytesIO
from utils.captcha import Captcha
import qiniu
import config
from tasks import send_sms_captcha

# 注意设置url前缀的时候一定要加入'/'
bp = Blueprint("common",__name__,url_prefix='/c')

# @bp.route('/sms_captcha/')
# def sms_captcha():
#     telephone = request.args.get('telephone')
#     if not telephone:
#         return restful.params_error(message="请传入手机号码")
#     # result = alidayu.send_sms("+86-188923326060",code="abcd")
#     captcha = Captcha.gene_text(4)
#     if demo_sms_send.send_api(telephone,code=captcha):
#         return restful.success()
#     else:
#         return restful.params_error(message="短信验证码发送失败！")


@bp.route('/sms_captcha/',methods=['POST'])
def sms_captcha():
    # telephone
    # timestamp
    # md5(ts+telephone+salt)
    form = SMSCaptchaForm(request.form)
    if form.validate():
        telephone = form.telephone.data
        captcha = Captcha.gene_text(4)
        print("发送的短信验证码是:",captcha)
        send_sms_captcha(telephone=telephone,captcha=captcha)
        return restful.success()
        # if demo_sms_send.send_api(telephone,code=captcha):
        #     # 利用memcached进行缓存
        #     zlcache.set(telephone,captcha)
        #     return restful.success()
        # else:
        #     return restful.params_error(message="短信验证码发送失败！")
    else:
        return restful.params_error(message="参数错误")


@bp.route('/captcha/')
def graph_captcha():
    try:
        # 获取验证码
        text,image = Captcha.gene_graph_captcha()
        zlcache.set(text.lower(),text.lower())
        # BytesIO,字节流
        # 获取图像，需要存入二进制流中
        out = BytesIO()
        # 保存在流中，并指定格式为png
        image.save(out,'png')
        out.seek(0)
        # response对象
        resp = make_response(out.read())
        resp.content_type = 'image/png'
    except:
        return graph_captcha()
    return resp


@bp.route('/uptoken/')
def uptoken():
    # AK
    access_key = config.UEDITOR_QINIU_ACCESS_KEY
    # SK
    secret_key = config.UEDITOR_QINIU_SECRET_KEY
    # 验证
    q = qiniu.Auth(access_key,secret_key)
    # 储存空间名字
    bucket = config.UEDITOR_QINIU_BUCKET_NAME
    token = q.upload_token(bucket)
    return jsonify({"uptoken":token})