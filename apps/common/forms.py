from apps.forms import BaseForm
from wtforms import StringField
from wtforms.validators import regexp,InputRequired
import hashlib
class SMSCaptchaForm(BaseForm):
    salt = "A@^JIJw55a7832a@kddad@！&12="
    telephone = StringField(validators=[regexp(r'^1[345789]\d{9}')])
    timestamp = StringField(validators=[regexp(r'\d{13}')])
    sign = StringField(validators=[InputRequired()])

    def validate(self):
        result = super(SMSCaptchaForm,self).validate()
        if not result:
            return False
        telephone = self.telephone.data
        timestamp = self.timestamp.data
        sign = self.sign.data
        # md5(timestamp+telephone+salt)
        # md5函数必须要传入一个bytes类型的字符串进去
        # 从md5对象中获取字符串需要调用hexdigest()方法
        sign2 = hashlib.md5((timestamp+telephone+self.salt).encode('utf-8')).hexdigest()
        print("客户端提交的",sign)
        print("服务器提交的",sign2)
        if sign == sign2:
            return True
        else:
            False
