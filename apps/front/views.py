from flask import (
    Blueprint,
    views,
    render_template,
    request,
    session,
    url_for,
    g,
    abort
)
from .forms import SignupForm,SigninForm,AddPostForm,AddCommentForm
from utils import restful,safeutils
from .models import FrontUser
from ..models import BoardModel,BannerModel,PostModel,CommentModel,HighlightPostModel,LikePostModel
from exts import db
import config
from .decorators import login_required
# from exts import alidayu
from flask_paginate import Pagination,get_page_parameter
from sqlalchemy.sql import func

bp = Blueprint("front",__name__)

@bp.route('/')
def index():
    sort = request.args.get("st",type=int,default=1) # 排序
    board_id = request.args.get("board_id",type=int,default=None)
    boards = BoardModel.query.all()
    banners = BannerModel.query.order_by(BannerModel.priority.desc(),BannerModel.create_time.desc()).limit(4)
    # posts = PostModel.query.all()
    page = request.args.get(get_page_parameter(),type=int,default=1)
    offset = (page-1) * config.PER_PAGE
    count = offset + config.PER_PAGE
    # 从第几条开始显示第end条数据
    # posts = PostModel.query.slice(offset,count)
    posts = None
    total = 0
    query_obj = None

    if sort == 1:
        # 最新
        query_obj = PostModel.query.order_by(PostModel.create_time.desc())
    elif sort == 2:
        # 按加精时间倒序排序
        # 两个表连接查询需要使用db.session()
        query_obj = db.session.query(PostModel).outerjoin(HighlightPostModel).order_by(HighlightPostModel.create_time.desc(),PostModel.create_time.desc())
    elif sort == 3:
        # 按照点赞数量排序
        query_obj = db.session.query(PostModel).outerjoin(LikePostModel).order_by(LikePostModel.like_num.desc(),PostModel.create_time.desc())
    elif sort == 4:
        query_obj = db.session.query(PostModel).outerjoin(CommentModel).group_by(PostModel.id).order_by(func.count(CommentModel.id).desc(),PostModel.create_time.desc())

    if board_id:
        # 按照版块筛选
        query_obj = PostModel.query.filter(PostModel.board_id==board_id)
        posts = query_obj.slice(offset,count)
        total = query_obj.count()
    else:
        posts = query_obj.slice(offset,count)
        total = query_obj.count()
    # 传入页数参数，数据的总条数
    pagination = Pagination(bs_version=3,page=page,total=total,outer_window=0,inner_window=2)
    context = {
        "boards":boards,
        "banners":banners,
        'posts':posts,
        'pagination':pagination,
        'current_board':board_id,
        'current_sort':sort,
        'like_obj':db.session.query(LikePostModel).outerjoin(PostModel),
    }
    return render_template("front/index.html",**context)

@bp.route('/<name>/<post_id>/')
def post_detail(name,post_id):
    post = PostModel.query.get(post_id)
    like = db.session.query(LikePostModel).outerjoin(PostModel).filter(PostModel.id == post_id).first()
    if not post:
        abort(404)
    return render_template('front/pdetail.html',post=post,like=like)

@bp.route('/acomment/',methods=["POST"])
@login_required
def add_comment():
    form = AddCommentForm(request.form)
    if form.validate():
        content = form.content.data
        # 通过帖子id，判断是否有当前帖子
        post_id = form.post_id.data
        post = PostModel.query.get(post_id)
        if post:
            comment = CommentModel(content=content)
            comment.post = post
            comment.author = g.front_user
            db.session.add(comment)
            db.session.commit()
            return restful.success()
        else:
            return  restful.params_error("没有这篇帖子！")
    else:
        return restful.params_error(form.get_error())

@bp.route('/apost/',methods=['GET','POST'])
@login_required
def apost():
    if request.method == "GET":
        boards = BoardModel.query.all()
        return render_template('front/apost.html',boards=boards)
    else:
        form = AddPostForm(request.form)
        if form.validate():
            title = form.title.data
            content = form.content.data
            board_id = form.board_id.data
            board = BoardModel.query.get(board_id)
            if not board:
                return restful.params_error(message="没有这个模块！")
            post = PostModel(title=title,content=content)
            post.board = board
            post.author = g.front_user
            db.session.add(post)
            db.session.commit()
            return  restful.success()
        else:
            return restful.params_error(message=form.get_error())

@bp.route('/alike/')
def add_like():
    post_id = request.args.get("post_id")
    post = PostModel.query.get(post_id)
    if not post:
        return restful.params_error("没有这篇帖子！")
    like = db.session.query(LikePostModel).outerjoin(PostModel).filter(PostModel.id==post_id).first()
    if like:
        like_num = like.like_num
    else:
        like = LikePostModel(like_num=0)
        like.post = post
        like_num = 0
    like_num = like_num+1
    print(like_num)
    like.like_num = like_num
    db.session.commit()
    return restful.success()

class SignupView(views.MethodView):

    def get(self):
        return_to = request.referrer
        print(return_to)
        if return_to and return_to != request.url and safeutils.is_safe_url(return_to):
            return render_template('front/signup.html',return_to=return_to)
        else:
            return render_template('front/signup.html')

    def post(self):
        form = SignupForm(request.form)
        if form.validate():
            telephone = form.telephone.data
            username = form.username.data
            password = form.password1.data
            user = FrontUser(telephone=telephone,username=username,password=password)
            db.session.add(user)
            db.session.commit()
            return restful.success()
        else:
            # print(form.get_error())
            return restful.params_error(message=form.get_error())

class SigninView(views.MethodView):

    def get(self):
        return_to = request.referrer
        if return_to and return_to != request.url and return_to != url_for("front.signup") and safeutils.is_safe_url(return_to):
             return render_template("front/signin.html",return_to=return_to)
        else:
             return render_template('front/signin.html')

    def post(self):
        form = SigninForm(request.form)
        if form.validate():
            telephone = form.telephone.data
            password = form.password.data
            remember = form.remeber.data
            # 检索是否拥有这个账号
            user = FrontUser.query.filter_by(telephone=telephone).first()
            if user and user.check_password(password):
                session[config.FRONT_USER_ID] = user.id
                if remember:
                    session.permanent = True
                return restful.success()
            else:
                return restful.params_error(message="手机号码或者密码错误")
        else:
            return restful.params_error(message=form.get_error())


bp.add_url_rule(rule='/signup/',view_func=SignupView.as_view('signup'))
bp.add_url_rule(rule='/signin/',view_func=SigninView.as_view('signin'))