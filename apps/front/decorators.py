from flask import session,redirect,url_for,g
from functools import wraps
import config

# @wraps(func)，将参数包装起来，避免数据丢失

# 登录验证判断
def login_required(func):
    @wraps(func)
    def inner(*args,**kwargs):
        if config.FRONT_USER_ID in session:
           return func(*args,**kwargs)
        else:
            return redirect(url_for('front.signin'))
    return inner
