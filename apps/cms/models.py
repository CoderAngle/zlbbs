from exts import db
from datetime import datetime
# 加密函数, generate_password_hash哈希,check_password_hash检测密码是否一致
from werkzeug.security import  generate_password_hash,check_password_hash

class CMSPersmission(object):
    # 255的二进制方式来表示 1111 1111
    ALL_PERMISSION = 0b11111111
    # 1. 访问者权限
    VISITOR =        0b00000001
    # 2. 管理帖子权限
    POSTER =         0b00000010
    # 3. 管理评论的权限
    COMMENTER =      0b00000100
    # 4. 管理板块的权限
    BOARDER =        0b00001000
    # 5. 管理前台用户的权限
    FRONTUSER =      0b00010000
    # 6. 管理后台用户的权限
    CMSUSER =        0b00100000
    # 7. 管理后台管理员的权限
    ADMINER =        0b01000000

# 绑定两个表，表与表之间的关系为多对多
# 引用外键，表名.键名
cms_role_user = db.Table(
    'cms_role_user',
    db.Column('cms_role_id',db.Integer,db.ForeignKey('cms_role.id'),primary_key=True),
    db.Column('cms_user_id',db.Integer,db.ForeignKey('cms_user.id'),primary_key=True),
)

class CMSRole(db.Model):
    __tablename__ = "cms_role"
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    name = db.Column(db.String(50),nullable=False)
    # 描述信息
    desc = db.Column(db.String(200),nullable=True)
    create_time = db.Column(db.DateTime,default=datetime.now)
    # 权限，默认为访问者权限
    permissions = db.Column(db.Integer,default=CMSPersmission.VISITOR)

    # 定义关系,users,rols,分别是两个表对应哪个的字段，secondary指定中间表
    users = db.relationship('CMSUser',secondary=cms_role_user,backref='roles')

# 创建cms用户数据库模型
class CMSUser(db.Model):
    # 设置表名
    __tablename__ = 'cms_user'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    username = db.Column(db.String(50),nullable=False)
    _password = db.Column(db.String(100),nullable=False)
    email = db.Column(db.String(50),nullable=False,unique=True)
    join_time = db.Column(db.DateTime,default=datetime.now)

    def __init__(self,username,password,email):
        self.username = username
        self.password = password
        self.email = email

    # getter/setter
    # 可以将一个方法定义成属性
    @property
    def password(self):
        return self._password

    # 重新定义设置方法
    @password.setter
    def password(self,raw_password):
        # 加密
        self._password = generate_password_hash(raw_password)

    def check_password(self,raw_password):
        # 原始密码和加密后的密码是否一致
        result = check_password_hash(self._password,raw_password)
        return result

    # 注意roles是什么，是上面的反向这个表的，用于通过这个属性绑定CMSRole模型
    # 拿到用户所有的权限
    @property
    def permissions(self):

        # 没有任何权限
        if not self.roles:
            return 0
        all_permissions = 0
        # 遍历权限
        for role in self.roles:
            permissions = role.permissions
            all_permissions |= permissions
        return all_permissions

    # 判断用户权限
    def has_permission(self,permission):
        # all_permissions = self.permissions
        # result =  all_permissions & permission == permission
        # return result
        return self.permissions & permission == permission

    # 判断是否为开发者
    @property
    def is_developer(self):
        return self.has_permission(CMSPersmission.ALL_PERMISSION)


# 重写密码字段
# 密码:对外的字段名叫做password
#      对内的字段名叫做"_password"
