from flask import (
    Blueprint,
    views,
    render_template,
    request,
    session,
    redirect,
    url_for,
    g,
    jsonify
)
from .forms import (
    LoginForm,
    ResetpwdForm,
    ResetEmailForm,
    AddBannerForm,
    UpdateBannerForm,
    AddBoardForm,
    UpdateBoardForm,
)
from .models import CMSUser,CMSPersmission
from ..models import BannerModel,BoardModel,PostModel,HighlightPostModel
from .decorators import login_required,permission_requeired
import config
from exts import db,mail
from flask_mail import Message
from utils import restful,zlcache
import string
import random
from flask_paginate import Pagination,get_page_parameter
from tasks import send_mail

# 创建蓝图
# subdomain:子域名，需要做映射
bp = Blueprint("cms",__name__,url_prefix='/cms')

@bp.route('/')
@login_required
def index():
    return render_template('cms/index.html')


@bp.route('/logout/')
@login_required
def logout():
    # 清除指定会话
    # del session[config.config.CMS_USER_ID]
    # 清除所有会话
    session.clear()
    # 重定向+反转
    return redirect(url_for('cms.login'))

@bp.route('/profile/')
@login_required
def profile():
    return render_template('cms/profile.html')

@bp.route('/email_captcha/')
@login_required
def email_captcha():
    # /email_captcha/?email_capthca=xxx@qq.com
    email = request.args.get('email')
    if not email:
        return restful.params_error("请传递邮箱参数")
    # string.ascii_letters:返回a~z和A~Z的所有字母
    # 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    source = list(string.ascii_letters)
    source.extend(map(lambda x:str(x),range(10)))
    # 随机抽取6为数
    captcha = random.sample(source,6)
    captcha = "".join([k for k in captcha])

    # 给邮箱发送邮件
    # message = Message(subject="python论坛邮箱验证码",recipients=[email],body=captcha)
    # try:
    #     mail.send(message)
    # except:
    #     return restful.params_error()
    # key:email
    # value:captch
    send_mail.delay(subject="python论坛邮箱验证码",recipients=[email],body=captcha)
    zlcache.set(email,captcha)
    return restful.success()

@bp.route('/email/')
def send_email():
    # subject:主题
    # recipients:接收者，列表类型，可以有多个接收者
    # body:发送的邮件的内容
    # sender:发送者，这个参数，可以修改发送者
    message = Message(subject='邮件发送',recipients=['1479852727@qq.com'],body='测试')
    mail.send(message)
    return "发送成功"

# 权限判断，防止跨越访问
@bp.route('/posts/')
@login_required
@permission_requeired(CMSPersmission.POSTER)
def posts():
    # posts_list = PostModel.query.all()
    page = request.args.get(get_page_parameter(),type=int,default=1)
    offset = (page-1) * config.PER_PAGE
    count = offset + config.PER_PAGE
    posts_list = PostModel.query.slice(offset,count)
    total = PostModel.query.count()
    pagination = Pagination(bs_version=3,page=page,total=total,outer_window=0,inner_window=2)
    context = {
        "posts":posts_list,
        "pagination":pagination,
    }

    return render_template('cms/posts.html',**context)

# 帖子加精
@bp.route('/hpost/',methods=['POST'])
@login_required
@permission_requeired(CMSPersmission.POSTER)
def hpost():
    post_id = request.form.get("post_id")
    if not post_id:
        return restful.params_error("请传入帖子id！")
    post = PostModel.query.get(post_id)
    if not post:
        return restful.params_error("没有这篇帖子！")
    highlight = HighlightPostModel()
    highlight.post = post
    db.session.add(highlight)
    db.session.commit()
    return restful.success()

@bp.route('/uhpost/',methods=["POST"])
@login_required
@permission_requeired(CMSPersmission.POSTER)
def uhpost():
    post_id = request.form.get("post_id")
    if not post_id:
        return restful.params_error("请传入帖子id！")
    post = PostModel.query.get(post_id)
    if not post:
        return restful.params_error("没有这篇帖子！")
    highlight = HighlightPostModel.query.filter_by(post_id=post_id).first()
    db.session.delete(highlight)
    db.session.commit()
    return restful.success()

@bp.route('/comments/')
@login_required
@permission_requeired(CMSPersmission.COMMENTER)
def comments():
    return render_template('cms/comments.html')

@bp.route('/boards/')
@login_required
@permission_requeired(CMSPersmission.BOARDER)
def boards():
    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (page - 1) * config.PER_PAGE
    count = offset + config.PER_PAGE
    total = PostModel.query.count()
    pagination = Pagination(bs_version=3, page=page, total=total, outer_window=0, inner_window=2)
    board_models = BoardModel.query.slice(offset, count)
    context = {
        "boards":board_models,
        "pagination":pagination,
    }
    return render_template('cms/boards.html',**context)

# 添加版块
@bp.route('/aboard/',methods=['POST'])
@login_required
@permission_requeired(CMSPersmission.BOARDER)
def aboard():
    form = AddBoardForm(request.form)
    if form.validate():
        name = form.name.data
        board = BoardModel(name=name)
        db.session.add(board)
        db.session.commit()
        return restful.success()
    else:
        return restful.params_error(message=form.get_error())

# 编辑版块
@bp.route('/uboard/',methods=["POST"])
@login_required
@permission_requeired(CMSPersmission.BOARDER)
def uboard():
    form = UpdateBoardForm(request.form)
    if form.validate():
        board_id = form.board_id.data
        name = form.name.data
        board = BoardModel.query.get(board_id)
        if board:
            board.name = name
            db.session.commit()
            return restful.success()
        else:
            return restful.params_error(message="没有这个版块")
    else:
        return restful.params_error(message=form.get_error())

# 删除版块
@bp.route('/dboard/',methods=["POST"])
@login_required
@permission_requeired(CMSPersmission.BOARDER)
def dboard():
    board_id = request.form.get("board_id")
    if not board_id:
        return restful.params_error(message="请传入版块id！")
    board = BoardModel.query.get(board_id)
    if not board:
        return restful.params_error(message="没有这个版块！")
    db.session.delete(board)
    db.session.commit()
    return restful.success()

@bp.route('fusers')
@login_required
@permission_requeired(CMSPersmission.FRONTUSER)
def fusers():
    return render_template('cms/fusers.html')

@bp.route('/cusers/')
@login_required
@permission_requeired(CMSPersmission.CMSUSER)
def cusers():
    return render_template('cms/cusers.html')

@bp.route('/croles/')
@login_required
@permission_requeired(CMSPersmission.ALL_PERMISSION)
def croles():
    return render_template('cms/croles.html')

@bp.route('/banners/')
@login_required
def banners():
    # banners = BannerModel.query.order_by(BannerModel.priority.desc(),BannerModel.create_time.desc()).all()
    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (page - 1) * config.PER_PAGE
    count = offset + config.PER_PAGE
    total = BannerModel.query.count()
    pagination = Pagination(bs_version=3, page=page, total=total, outer_window=0, inner_window=2)
    banners = BannerModel.query.order_by(BannerModel.priority.desc(), BannerModel.create_time.desc()).slice(offset,count)
    context = {
        "banners":banners,
        "pagination":pagination,
    }
    return render_template('cms/banners.html',**context)

@bp.route('/abanner/',methods=['POST'])
@login_required
def abanner():
    form = AddBannerForm(request.form)
    if form.validate():
        name = form.name.data
        image_url = form.image_url.data
        link_url = form.link_url.data
        priority = form.priority.data
        banner = BannerModel(name=name,image_url=image_url,link_url=link_url,priority=priority)
        db.session.add(banner)
        db.session.commit()
        return restful.success()
    else:
        return restful.params_error(message=form.get_error())

@bp.route("/ubanner/",methods=["POST"])
@login_required
def ubanner():
    form = UpdateBannerForm(request.form)
    if form.validate():
        banner_id = form.banner_id.data
        name = form.name.data
        image_url = form.image_url.data
        link_url = form.link_url.data
        priority = form.priority.data
        banner = BannerModel.query.get(banner_id)
        if banner:
            banner.name = name
            banner.image_url = image_url
            banner.link_url = link_url
            banner.priority = priority
            db.session.commit()
            return restful.success()
        else:
            return restful.params_error(message="没有这个轮播图")
    else:
        return restful.params_error(message=form.get_error())

@bp.route('/dbanner/',methods=['POST'])
@login_required
def dbanner():
    banner_id = request.form.get("banner_id")
    if not banner_id:
        return restful.params_error(message="请传入轮播图id")
    banner = BannerModel.query.get(banner_id)
    if not banner:
        return restful.params_error(message="没有这个轮播图")
    db.session.delete(banner)
    db.session.commit()
    return restful.success()

# 采用类视图,重写get|post方法
class LoginView(views.MethodView):

    def get(self,message=None):
        return render_template('cms/login.html',message=message)

    def post(self):
        # 验证表单
        form = LoginForm(request.form)
        if form.validate():
            email = form.email.data
            password = form.password.data
            remember = form.remember.data
            user = CMSUser.query.filter_by(email=email).first()
            if user and user.check_password(password):
                session[config.CMS_USER_ID] = user.id
                if remember:
                    # 持久化
                    # 如果设置session.parmanet=True，那么过期时间是31天
                    session.permanent = True
                # 反转蓝图规则:蓝图名.视图
                return redirect(url_for('cms.index'))
            else:
                return self.get(message="邮箱或密码错误")
        else:
            # print(form.errors)
            message = form.get_error()
            return self.get(message=message)

class ResetPwdView(views.MethodView):

    # 给类视图定义装饰器
    decorators = [login_required]

    def get(self):
        return render_template('cms/resetpwd.html')

    def post(self):
        form = ResetpwdForm(request.form)
        if form.validate():
            # 获取密码信息
            oldpwd = form.oldpwd.data
            newpwd = form.newpwd.data
            # 获取全局对象g的用户属性
            user = g.cms_user
            # 判断原密码是否一致
            if user.check_password(oldpwd):
                # 修改密码
                user.password = newpwd
                # 提交到数据库中
                db.session.commit()
                # 返回数据
                return restful.success()
            else:
                # return jsonify({"code":400,"message":"旧密码错误"})
                return restful.params_error('旧密码错误!')
        else:
            message = form.get_error()
            # return jsonify({'code':400,"message":message})
            return restful.params_error(form.get_error())

class ResetEmailView(views.MethodView):

    decorators = [login_required]
    def get(self):
        return render_template('cms/resetemail.html')

    def post(self):
        print(request.form)
        form = ResetEmailForm(request.form)
        if form.validate():
            email = form.email.data
            g.cms_user.email = email
            db.session.commit()
            return restful.success()
        else:
            return restful.params_error(form.get_error())


# 添加视图
bp.add_url_rule('/login/',view_func=LoginView.as_view('login'))
bp.add_url_rule('/resetpwd/',view_func=ResetPwdView.as_view('resetpwd'))
bp.add_url_rule('/resetemail/',view_func=ResetEmailView.as_view('resetemail'))
