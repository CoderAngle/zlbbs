from wtforms import Form,StringField,IntegerField
from wtforms.validators import  Email,InputRequired,Length,EqualTo
from apps.forms import BaseForm
from utils import zlcache
from wtforms import ValidationError
from flask import g

# 验证登录表单
class LoginForm(BaseForm):
    email = StringField(validators=[Email(message="请输入正确的邮箱格式")])
    password = StringField(validators=[Length(6,20,message="请输入正确格式的密码")])
    remember = IntegerField()

# 验证更改密码的表单
class ResetpwdForm(BaseForm):
    oldpwd = StringField(validators=[Length(6,20,message="请输入正确格式的密码")])
    newpwd = StringField(validators=[Length(6,20,message="请输入正确格式的密码")])
    newpwd2 = StringField(validators=[EqualTo("newpwd")])

class ResetEmailForm(BaseForm):
    email = StringField(validators=[Email(message='请输入正确格式的邮箱！')])
    captcha = StringField(validators=[Length(6,20)])

    def validate_captcha(self,field):
        print(field)
        captcha = field.data
        email = self.email.data
        captcha_cache = zlcache.get(email)
        print(captcha.lower(),captcha_cache.lower())
        if not captcha_cache or captcha.lower() != captcha_cache.lower():
            raise ValidationError('邮箱验证码错误！')

    def validate_email(self,field):
        email = field.data
        user = g.cms_user
        if user.email == email:
            raise ValidationError('不能修改为相同的邮箱！')

class AddBannerForm(BaseForm):
    name = StringField(validators=[InputRequired(message="请输入轮播图名称！")])
    image_url = StringField(validators=[InputRequired(message="请输入轮播图图片链接！")])
    link_url = StringField(validators=[InputRequired(message="请输入轮播图跳转链接！")])
    priority = IntegerField(validators=[InputRequired(message="请输入轮播图优先级！")])

class UpdateBannerForm(AddBannerForm):
    banner_id = IntegerField(validators=[InputRequired(message="请输入轮播图的id！")])

class AddBoardForm(BaseForm):
    name = StringField(validators=[InputRequired(message="请输入版块名称！")])

class UpdateBoardForm(AddBoardForm):
    board_id = StringField(validators=[InputRequired(message="请输入模块id！")])