from flask import session,redirect,url_for,g
from functools import wraps
import config

# @wraps(func)，将参数包装起来，避免数据丢失

# 登录验证判断
def login_required(func):
    @wraps(func)
    def inner(*args,**kwargs):
        if config.CMS_USER_ID in session:
           return func(*args,**kwargs)
        else:
            return redirect(url_for('cms.login'))
    return inner

# 权限验证
def permission_requeired(permission):
    def outter(func):
        @wraps(func)
        def inner(*args,**kwargs):
            user = g.cms_user
            # 判断用户是否具有访问权限
            if user.has_permission(permission):
                return func(*args,**kwargs)
            else:
                return redirect(url_for('cms.index'))
        return inner
    return outter
