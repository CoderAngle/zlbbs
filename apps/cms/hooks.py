from .views import bp
import config
from flask import  session,g
from .models import CMSUser,CMSPersmission

# 钩子函数，在请求之前获取
@bp.before_request
def before_request():
    if config.CMS_USER_ID in session:
        user_id = session.get(config.CMS_USER_ID)
        user = CMSUser.query.get(user_id)
        if user:
            g.cms_user = user

# 上下文，这样在bp蓝图下的模板都能够使用
@bp.context_processor
def cms_context_peocessor():
    return {"CMSPersmission":CMSPersmission}