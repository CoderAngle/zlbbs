from wtforms import Form

class BaseForm(Form):
    def get_error(self):
        try:
            message = self.errors.popitem()[1][0]
            print(message)
            if message:
                return message
            else:
                return None
        except:
                return None