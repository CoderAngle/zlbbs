-- MySQL dump 10.13  Distrib 5.7.22, for Win64 (x86_64)
--
-- Host: localhost    Database: zlbbs
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alembic_version`
--

DROP TABLE IF EXISTS `alembic_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alembic_version` (
  `version_num` varchar(32) NOT NULL,
  PRIMARY KEY (`version_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alembic_version`
--

LOCK TABLES `alembic_version` WRITE;
/*!40000 ALTER TABLE `alembic_version` DISABLE KEYS */;
INSERT INTO `alembic_version` VALUES ('d3faaeed45d4');
/*!40000 ALTER TABLE `alembic_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners_model`
--

DROP TABLE IF EXISTS `banners_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `link_url` varchar(255) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners_model`
--

LOCK TABLES `banners_model` WRITE;
/*!40000 ALTER TABLE `banners_model` DISABLE KEYS */;
INSERT INTO `banners_model` VALUES (1,'日本的生鲜零售 ','http://pdhjzz2pa.bkt.clouddn.com/o_1cku8dtar1n109ml1fhmdh41dlm7.png','https://www.bufanbiz.com/class/studyexpo/97.html?adf=top.banner',2,'2018-08-16 16:42:18'),(2,'美国金融科技行业','http://pdhjzz2pa.bkt.clouddn.com/o_1cku8eo3l127e1pe6i881dah1o0c7.jpg','https://www.bufanbiz.com/class/studyexpo/95.html?adf=top.banner',4,'2018-08-16 16:42:49'),(3,'不凡商业','http://pdhjzz2pa.bkt.clouddn.com/o_1cku8fqvro7s14v31vpe1u3b1kt37.png','https://mp.weixin.qq.com/s/NmQ6OXO3VT7ycjnBunMp-g?adf=top.banner',3,'2018-08-16 16:43:24'),(4,'小饭桌创业课堂','http://pdhjzz2pa.bkt.clouddn.com/o_1cku8hqmdffm49a2t2179o1857.jpg','https://www.bufanbiz.com/event/101.html?adf=top.banner',1,'2018-08-16 16:43:44');
/*!40000 ALTER TABLE `banners_model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `board`
--

DROP TABLE IF EXISTS `board`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `board` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `board`
--

LOCK TABLES `board` WRITE;
/*!40000 ALTER TABLE `board` DISABLE KEYS */;
INSERT INTO `board` VALUES (1,'python进阶','2018-08-16 16:44:07'),(2,'python全栈开发','2018-08-16 16:44:14'),(3,'django','2018-08-16 16:44:19'),(4,'前端开发','2018-08-16 16:44:26'),(5,'flask','2018-08-16 16:44:31'),(6,'python爬虫','2018-08-16 16:45:47');
/*!40000 ALTER TABLE `board` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_role`
--

DROP TABLE IF EXISTS `cms_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `desc` varchar(200) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `permissions` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_role`
--

LOCK TABLES `cms_role` WRITE;
/*!40000 ALTER TABLE `cms_role` DISABLE KEYS */;
INSERT INTO `cms_role` VALUES (1,'访问者','只能阅读相关数据，不能修改。','2018-08-16 16:38:49',1),(2,'运营','管理帖子，管理评论,管理前台用户。','2018-08-16 16:38:50',55),(3,'管理员','拥有本系统所有权限。','2018-08-16 16:38:50',63),(4,'开发者','开发人员专用角色。','2018-08-16 16:38:50',255);
/*!40000 ALTER TABLE `cms_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_role_user`
--

DROP TABLE IF EXISTS `cms_role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_role_user` (
  `cms_role_id` int(11) NOT NULL,
  `cms_user_id` int(11) NOT NULL,
  PRIMARY KEY (`cms_role_id`,`cms_user_id`),
  KEY `cms_user_id` (`cms_user_id`),
  CONSTRAINT `cms_role_user_ibfk_1` FOREIGN KEY (`cms_role_id`) REFERENCES `cms_role` (`id`),
  CONSTRAINT `cms_role_user_ibfk_2` FOREIGN KEY (`cms_user_id`) REFERENCES `cms_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_role_user`
--

LOCK TABLES `cms_role_user` WRITE;
/*!40000 ALTER TABLE `cms_role_user` DISABLE KEYS */;
INSERT INTO `cms_role_user` VALUES (1,1),(2,2),(3,3),(4,4);
/*!40000 ALTER TABLE `cms_role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_user`
--

DROP TABLE IF EXISTS `cms_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `_password` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `join_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_user`
--

LOCK TABLES `cms_user` WRITE;
/*!40000 ALTER TABLE `cms_user` DISABLE KEYS */;
INSERT INTO `cms_user` VALUES (1,'我是访问者','pbkdf2:sha256:50000$jWHvRy2s$4456da7b0d2dccc9d217cfc42610f996ec0774c19f1df131f11cde612fb301f8','visitor@qq.com','2018-08-16 16:39:43'),(2,'我是运营者','pbkdf2:sha256:50000$l3tLKZQm$61c1e409be40820bebb251f33ce91bd968b05385828c7bae612872ac8ed34894','operator@qq.com','2018-08-16 16:39:52'),(3,'我是管理员','pbkdf2:sha256:50000$yVNsKT0f$6ff14096b3117a3ab3ee0d1c2a4efc2b78c6535a8e0ec6c47e1f85d945c16d44','1479852727@qq.com','2018-08-16 16:40:01'),(4,'我是开发者','pbkdf2:sha256:50000$1KYNSSc8$691a2f697d5cb294865ddf93201ba2b636f156a092a0cef89fb0019ff3c6a5e2','developer@qq.com','2018-08-16 16:40:19');
/*!40000 ALTER TABLE `cms_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `author_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `front_user` (`id`),
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,'<p>123</p>','2018-08-17 15:44:18',2,'gkK7W3pYiroYwEMbgSdCR8'),(2,'<p>angle</p>','2018-08-17 15:46:30',2,'gkK7W3pYiroYwEMbgSdCR8'),(3,'<p>来一起学flask</p>','2018-08-17 16:09:16',2,'gkK7W3pYiroYwEMbgSdCR8'),(4,'<p>1</p>','2018-08-17 21:00:07',3,'gkK7W3pYiroYwEMbgSdCR8'),(5,'<p>5</p>','2018-08-17 21:00:14',3,'gkK7W3pYiroYwEMbgSdCR8'),(6,'<p>1</p>','2018-08-17 21:02:59',3,'gkK7W3pYiroYwEMbgSdCR8'),(7,'<p>好啊</p>','2018-08-17 22:23:48',2,'gkK7W3pYiroYwEMbgSdCR8'),(8,'<p>1</p>','2018-08-17 22:25:41',207,'gkK7W3pYiroYwEMbgSdCR8'),(9,'<p>111</p>','2018-08-18 20:50:47',208,'gkK7W3pYiroYwEMbgSdCR8'),(10,'<p>xada</p>','2018-08-18 20:51:13',208,'gkK7W3pYiroYwEMbgSdCR8');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_user`
--

DROP TABLE IF EXISTS `front_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_user` (
  `id` varchar(100) NOT NULL,
  `telephone` varchar(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `_password` varchar(100) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `realname` varchar(50) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `signature` varchar(100) DEFAULT NULL,
  `gender` enum('MALE','FEMALE','SECRET','UNKNOW') DEFAULT NULL,
  `join_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `telephone` (`telephone`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_user`
--

LOCK TABLES `front_user` WRITE;
/*!40000 ALTER TABLE `front_user` DISABLE KEYS */;
INSERT INTO `front_user` VALUES ('gkK7W3pYiroYwEMbgSdCR8','18892332606','angle','pbkdf2:sha256:50000$MtGHHLy7$99f9d0426d18d287539a034d5f17094b40b62da05fddabe53e0561fb8c0e15fb',NULL,NULL,NULL,NULL,'UNKNOW','2018-08-16 16:40:29');
/*!40000 ALTER TABLE `front_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `highlight_post`
--

DROP TABLE IF EXISTS `highlight_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `highlight_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `highlight_post_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `highlight_post`
--

LOCK TABLES `highlight_post` WRITE;
/*!40000 ALTER TABLE `highlight_post` DISABLE KEYS */;
INSERT INTO `highlight_post` VALUES (4,11,'2018-08-17 22:24:27');
/*!40000 ALTER TABLE `highlight_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `like_post`
--

DROP TABLE IF EXISTS `like_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `like_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `like_num` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `like_post_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `like_post`
--

LOCK TABLES `like_post` WRITE;
/*!40000 ALTER TABLE `like_post` DISABLE KEYS */;
INSERT INTO `like_post` VALUES (1,2,17,'2018-08-18 00:02:57'),(2,2,1,'2018-08-18 00:03:01'),(3,189,1,'2018-08-18 00:03:22'),(4,189,1,'2018-08-18 00:03:27'),(5,189,1,'2018-08-18 00:03:28'),(6,189,1,'2018-08-18 00:03:29'),(7,208,1,'2018-08-18 00:25:46'),(8,191,5,'2018-08-18 00:43:06'),(9,191,0,'2018-08-18 00:43:09'),(10,191,1,'2018-08-18 00:43:27'),(11,191,1,'2018-08-18 00:43:28'),(12,191,1,'2018-08-18 00:43:30'),(13,155,3,'2018-08-18 00:47:13'),(14,185,9,'2018-08-18 13:24:27'),(15,201,4,'2018-08-18 13:34:01');
/*!40000 ALTER TABLE `like_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `board_id` int(11) DEFAULT NULL,
  `author_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  KEY `board_id` (`board_id`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `front_user` (`id`),
  CONSTRAINT `post_ibfk_2` FOREIGN KEY (`board_id`) REFERENCES `board` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (2,'python3爬虫实战笔记','<h1 style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin: 8px 0px 16px; font-size: 28px; color: rgb(79, 79, 79); line-height: 36px; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\">介绍</h1><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\"><a href=\"https://github.com/CoderAngle/python3-spider-note\" target=\"_blank\" style=\"box-sizing: border-box; outline: 0px; color: rgb(103, 149, 181); text-decoration-line: none; cursor: pointer; word-break: break-all;\"><img src=\"https://img.shields.io/badge/python3%E7%88%AC%E8%99%AB%E7%AC%94%E8%AE%B0-%E5%BF%83%E8%B7%B3%E5%9B%9E%E5%BF%86-green.svg?longCache=true&style=plastic\" alt=\"Coveralls bitbucket branch\" title=\"\"/></a></p><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\">python使用版本:python3</p><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\">(python3网络爬虫开发实战笔记)</p><h2 style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin: 8px 0px 16px; font-size: 24px; color: rgb(79, 79, 79); line-height: 32px; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\"><a style=\"box-sizing: border-box; outline: 0px; color: rgb(78, 161, 219); cursor: pointer; word-break: break-all;\"></a>pip换源</h2><pre class=\"prettyprint\" style=\"box-sizing: border-box; outline: 0px; padding: 8px 16px 4px 56px; margin-top: 0px; margin-bottom: 24px; position: relative; overflow: auto hidden; font-family: Consolas, Inconsolata, Courier, monospace; font-size: 14px; line-height: 22px; word-break: break-all; background-color: rgb(246, 248, 250); border: none;\">pip源:\n豆瓣：http://pypi.douban.com/simple/\n清华：https://pypi.tuna.tsinghua.edu.cn/simple123</pre><h3 style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin: 8px 0px 16px; font-size: 22px; color: rgb(79, 79, 79); line-height: 30px; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\"><a style=\"box-sizing: border-box; outline: 0px; color: rgb(78, 161, 219); cursor: pointer; word-break: break-all;\"></a>第一种:暂时性换源</h3><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\">有时候pip下载速度有点慢，可以换成国内源提升一下速度</p><pre class=\"prettyprint\" style=\"box-sizing: border-box; outline: 0px; padding: 8px 16px 4px 56px; margin-top: 0px; margin-bottom: 24px; position: relative; overflow: auto hidden; font-family: Consolas, Inconsolata, Courier, monospace; font-size: 14px; line-height: 22px; word-break: break-all; background-color: rgb(246, 248, 250); border: none;\">pip&nbsp;install&nbsp;-i&nbsp;https://pypi.doubanio.com/simple/&nbsp;包名&nbsp;\n\npip&nbsp;install&nbsp;-i&nbsp;https://pypi.doubanio.com/simple/&nbsp;flask123</pre><h3 style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin: 8px 0px 16px; font-size: 22px; color: rgb(79, 79, 79); line-height: 30px; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\"><a style=\"box-sizing: border-box; outline: 0px; color: rgb(78, 161, 219); cursor: pointer; word-break: break-all;\"></a>第二种:永久换源</h3><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\">永久换源的方法如下:</p><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\">linux下，修改 ~/.pip/pip.conf (没有就创建一个)， 修改 index-url，内容如下：</p><pre class=\"prettyprint\" style=\"box-sizing: border-box; outline: 0px; padding: 8px 16px 4px 56px; margin-top: 0px; margin-bottom: 24px; position: relative; overflow: auto hidden; font-family: Consolas, Inconsolata, Courier, monospace; font-size: 14px; line-height: 22px; word-break: break-all; background-color: rgb(246, 248, 250); border: none;\">[global]\n\nindex-url&nbsp;=&nbsp;https://pypi.tuna.tsinghua.edu.cn/simple123</pre><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\">windows下，直接在用户目录中创建一个pip目录，如：C:\\Users\\miku\\pip，新建文件pip.ini，内容如下</p><pre class=\"prettyprint\" style=\"box-sizing: border-box; outline: 0px; padding: 8px 16px 4px 56px; margin-top: 0px; margin-bottom: 24px; position: relative; overflow: auto hidden; font-family: Consolas, Inconsolata, Courier, monospace; font-size: 14px; line-height: 22px; word-break: break-all; background-color: rgb(246, 248, 250); border: none;\">[global]\n\nindex-url&nbsp;=&nbsp;https://pypi.tuna.tsinghua.edu.cn/simple123</pre><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\"><img src=\"https://blog.csdn.net/only_Tokimeki/article/details/.gitbook/assets/000.png\" alt=\"\" title=\"\"/></p><h2 style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin: 8px 0px 16px; font-size: 24px; color: rgb(79, 79, 79); line-height: 32px; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\"><a style=\"box-sizing: border-box; outline: 0px; color: rgb(78, 161, 219); cursor: pointer; word-break: break-all;\"></a>IDE工具</h2><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\">为了提升编码速度，需要好的编译器，推荐使用pycharm</p><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\">以下是pycharm破解,使用2017版本就可以了</p><pre class=\"prettyprint\" style=\"box-sizing: border-box; outline: 0px; padding: 8px 16px 4px 56px; margin-top: 0px; margin-bottom: 24px; position: relative; overflow: auto hidden; font-family: Consolas, Inconsolata, Courier, monospace; font-size: 14px; line-height: 22px; word-break: break-all; background-color: rgb(246, 248, 250); border: none;\">http://idea.lanyus.com/-javaagent:&nbsp;E:\\JetBrains\\WebStorm&nbsp;2018.1\\lib\\JetbrainsCrack-2.7-release-strThisCrackLicenseId-{&quot;licenseId&quot;:&quot;ThisCrackLicenseId&quot;,&quot;licenseeName&quot;:&quot;idea&quot;,&quot;assigneeName&quot;:&quot;&quot;,&quot;assigneeEmail&quot;:&quot;idea@163.com&quot;,&quot;licenseRestriction&quot;:&quot;For&nbsp;This&nbsp;Crack,&nbsp;Only&nbsp;Test!&nbsp;Please&nbsp;support&nbsp;genuine!!!&quot;,&quot;checkConcurrentUse&quot;:false,&quot;products&quot;:[\n{&quot;code&quot;:&quot;II&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;},\n{&quot;code&quot;:&quot;DM&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;},\n{&quot;code&quot;:&quot;AC&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;},\n{&quot;code&quot;:&quot;RS0&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;},\n{&quot;code&quot;:&quot;WS&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;},\n{&quot;code&quot;:&quot;DPN&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;},\n{&quot;code&quot;:&quot;RC&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;},\n{&quot;code&quot;:&quot;PS&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;},\n{&quot;code&quot;:&quot;DC&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;},\n{&quot;code&quot;:&quot;RM&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;},\n{&quot;code&quot;:&quot;CL&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;},\n{&quot;code&quot;:&quot;PC&quot;,&quot;paidUpTo&quot;:&quot;2099-12-31&quot;}\n],&quot;hash&quot;:&quot;2911276/0&quot;,&quot;gracePeriodDays&quot;:7,&quot;autoProlongated&quot;:false}123456789101112131415161718192021222324252627282930</pre><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\"><a href=\"https://pan.baidu.com/s/1si1u-15AhTkWq2bRdkascQ%20\" target=\"_blank\" style=\"box-sizing: border-box; outline: 0px; color: rgb(103, 149, 181); text-decoration-line: none; cursor: pointer; word-break: break-all;\">下载地址</a>&nbsp;密码：v97f</p><h2 style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin: 8px 0px 16px; font-size: 24px; color: rgb(79, 79, 79); line-height: 32px; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\"><a style=\"box-sizing: border-box; outline: 0px; color: rgb(78, 161, 219); cursor: pointer; word-break: break-all;\"></a>Linux中切换python版本</h2><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\">首先先来看一下我们的默认Python版本</p><pre class=\"prettyprint\" style=\"box-sizing: border-box; outline: 0px; padding: 8px 16px 4px 56px; margin-top: 0px; margin-bottom: 24px; position: relative; overflow: auto hidden; font-family: Consolas, Inconsolata, Courier, monospace; font-size: 14px; line-height: 22px; word-break: break-all; background-color: rgb(246, 248, 250); border: none;\">$&nbsp;python&nbsp;--versionPython&nbsp;2.7.612</pre><p style=\"box-sizing: border-box; outline: 0px; padding: 0px; margin-top: 0px; margin-bottom: 16px; color: rgb(79, 79, 79); line-height: 26px; text-align: justify; word-break: break-all; font-family: -apple-system, &quot;SF UI Text&quot;, Arial, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif, SimHei, SimSun; white-space: normal; background-color: rgb(255, 255, 255);\">然后修改一下别名</p><pre class=\"prettyprint\" style=\"box-sizing: border-box; outline: 0px; padding: 8px 16px 4px 56px; margin-top: 0px; margin-bottom: 24px; position: relative; overflow: auto hidden; font-family: Consolas, Inconsolata, Courier, monospace; font-size: 14px; line-height: 22px; word-break: break-all; background-color: rgb(246, 248, 250); border: none;\">$&nbsp;alias&nbsp;python=&#39;/usr/bin/python3&#39;$&nbsp;python&nbsp;--versionPython&nbsp;3.4.3&nbsp;&nbsp;#&nbsp;版本已经改变</pre><p><br/></p>','2018-08-16 16:55:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(3,'标题1','内容:1','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(4,'标题2','内容:2','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(5,'标题3','内容:3','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(6,'标题4','内容:4','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(7,'标题5','内容:5','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(8,'标题6','内容:6','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(9,'标题7','内容:7','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(10,'标题8','内容:8','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(11,'标题9','内容:9','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(12,'标题10','内容:10','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(13,'标题11','内容:11','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(14,'标题12','内容:12','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(15,'标题13','内容:13','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(16,'标题14','内容:14','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(17,'标题15','内容:15','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(18,'标题16','内容:16','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(19,'标题17','内容:17','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(20,'标题18','内容:18','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(21,'标题19','内容:19','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(22,'标题20','内容:20','2018-08-16 17:08:22',1,'gkK7W3pYiroYwEMbgSdCR8'),(23,'标题21','内容:21','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(24,'标题22','内容:22','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(25,'标题23','内容:23','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(26,'标题24','内容:24','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(27,'标题25','内容:25','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(28,'标题26','内容:26','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(29,'标题27','内容:27','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(30,'标题28','内容:28','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(31,'标题29','内容:29','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(32,'标题30','内容:30','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(33,'标题31','内容:31','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(34,'标题32','内容:32','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(35,'标题33','内容:33','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(36,'标题34','内容:34','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(37,'标题35','内容:35','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(38,'标题36','内容:36','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(39,'标题37','内容:37','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(40,'标题38','内容:38','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(41,'标题39','内容:39','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(42,'标题40','内容:40','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(43,'标题41','内容:41','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(44,'标题42','内容:42','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(45,'标题43','内容:43','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(46,'标题44','内容:44','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(47,'标题45','内容:45','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(48,'标题46','内容:46','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(49,'标题47','内容:47','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(50,'标题48','内容:48','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(51,'标题49','内容:49','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(52,'标题50','内容:50','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(53,'标题51','内容:51','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(54,'标题52','内容:52','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(55,'标题53','内容:53','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(56,'标题54','内容:54','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(57,'标题55','内容:55','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(58,'标题56','内容:56','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(59,'标题57','内容:57','2018-08-16 17:08:23',1,'gkK7W3pYiroYwEMbgSdCR8'),(60,'标题58','内容:58','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(61,'标题59','内容:59','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(62,'标题60','内容:60','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(63,'标题61','内容:61','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(64,'标题62','内容:62','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(65,'标题63','内容:63','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(66,'标题64','内容:64','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(67,'标题65','内容:65','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(68,'标题66','内容:66','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(69,'标题67','内容:67','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(70,'标题68','内容:68','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(71,'标题69','内容:69','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(72,'标题70','内容:70','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(73,'标题71','内容:71','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(74,'标题72','内容:72','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(75,'标题73','内容:73','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(76,'标题74','内容:74','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(77,'标题75','内容:75','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(78,'标题76','内容:76','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(79,'标题77','内容:77','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(80,'标题78','内容:78','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(81,'标题79','内容:79','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(82,'标题80','内容:80','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(83,'标题81','内容:81','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(84,'标题82','内容:82','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(85,'标题83','内容:83','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(86,'标题84','内容:84','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(87,'标题85','内容:85','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(88,'标题86','内容:86','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(89,'标题87','内容:87','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(90,'标题88','内容:88','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(91,'标题89','内容:89','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(92,'标题90','内容:90','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(93,'标题91','内容:91','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(94,'标题92','内容:92','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(95,'标题93','内容:93','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(96,'标题94','内容:94','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(97,'标题95','内容:95','2018-08-16 17:08:24',1,'gkK7W3pYiroYwEMbgSdCR8'),(98,'标题96','内容:96','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(99,'标题97','内容:97','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(100,'标题98','内容:98','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(101,'标题99','内容:99','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(102,'标题100','内容:100','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(103,'标题101','内容:101','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(104,'标题102','内容:102','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(105,'标题103','内容:103','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(106,'标题104','内容:104','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(107,'标题105','内容:105','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(108,'标题106','内容:106','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(109,'标题107','内容:107','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(110,'标题108','内容:108','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(111,'标题109','内容:109','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(112,'标题110','内容:110','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(113,'标题111','内容:111','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(114,'标题112','内容:112','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(115,'标题113','内容:113','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(116,'标题114','内容:114','2018-08-16 17:08:25',1,'gkK7W3pYiroYwEMbgSdCR8'),(117,'标题115','内容:115','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(118,'标题116','内容:116','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(119,'标题117','内容:117','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(120,'标题118','内容:118','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(121,'标题119','内容:119','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(122,'标题120','内容:120','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(123,'标题121','内容:121','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(124,'标题122','内容:122','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(125,'标题123','内容:123','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(126,'标题124','内容:124','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(127,'标题125','内容:125','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(128,'标题126','内容:126','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(129,'标题127','内容:127','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(130,'标题128','内容:128','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(131,'标题129','内容:129','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(132,'标题130','内容:130','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(133,'标题131','内容:131','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(134,'标题132','内容:132','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(135,'标题133','内容:133','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(136,'标题134','内容:134','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(137,'标题135','内容:135','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(138,'标题136','内容:136','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(139,'标题137','内容:137','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(140,'标题138','内容:138','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(141,'标题139','内容:139','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(142,'标题140','内容:140','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(143,'标题141','内容:141','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(144,'标题142','内容:142','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(145,'标题143','内容:143','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(146,'标题144','内容:144','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(147,'标题145','内容:145','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(148,'标题146','内容:146','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(149,'标题147','内容:147','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(150,'标题148','内容:148','2018-08-16 17:08:26',1,'gkK7W3pYiroYwEMbgSdCR8'),(151,'标题149','内容:149','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(152,'标题150','内容:150','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(153,'标题151','内容:151','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(154,'标题152','内容:152','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(155,'标题153','内容:153','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(156,'标题154','内容:154','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(157,'标题155','内容:155','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(158,'标题156','内容:156','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(159,'标题157','内容:157','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(160,'标题158','内容:158','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(161,'标题159','内容:159','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(162,'标题160','内容:160','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(163,'标题161','内容:161','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(164,'标题162','内容:162','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(165,'标题163','内容:163','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(166,'标题164','内容:164','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(167,'标题165','内容:165','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(168,'标题166','内容:166','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(169,'标题167','内容:167','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(170,'标题168','内容:168','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(171,'标题169','内容:169','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(172,'标题170','内容:170','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(173,'标题171','内容:171','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(174,'标题172','内容:172','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(175,'标题173','内容:173','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(176,'标题174','内容:174','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(177,'标题175','内容:175','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(178,'标题176','内容:176','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(179,'标题177','内容:177','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(180,'标题178','内容:178','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(181,'标题179','内容:179','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(182,'标题180','内容:180','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(183,'标题181','内容:181','2018-08-16 17:08:27',1,'gkK7W3pYiroYwEMbgSdCR8'),(184,'标题182','内容:182','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(185,'标题183','内容:183','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(186,'标题184','内容:184','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(187,'标题185','内容:185','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(188,'标题186','内容:186','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(189,'标题187','内容:187','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(190,'标题188','内容:188','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(191,'标题189','内容:189','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(192,'标题190','内容:190','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(193,'标题191','内容:191','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(194,'标题192','内容:192','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(195,'标题193','内容:193','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(196,'标题194','内容:194','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(197,'标题195','内容:195','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(198,'标题196','内容:196','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(199,'标题197','内容:197','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(200,'标题198','内容:198','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(201,'标题199','内容:199','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(202,'标题200','内容:200','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(203,'标题201','内容:201','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(204,'标题202','内容:202','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(205,'标题203','内容:203','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(206,'标题204','内容:204','2018-08-16 17:08:28',1,'gkK7W3pYiroYwEMbgSdCR8'),(207,'爬虫','<p>spide</p>','2018-08-16 18:22:43',6,'gkK7W3pYiroYwEMbgSdCR8'),(208,'flask','<p>flask</p>','2018-08-16 18:22:57',5,'gkK7W3pYiroYwEMbgSdCR8');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-20 20:48:40
