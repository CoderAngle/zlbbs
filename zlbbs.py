from flask import Flask,render_template
from apps.cms import bp as cms_bp
from apps.front import bp as front_bp
from apps.common import bp as common_bp
from apps.ueditor import bp as ueditor_bp
import config
from exts import db,crsf,mail
from flask_wtf.csrf import CSRFProtect



app = Flask(__name__)
# 导入配置
app.config.from_object(config)
# 初始化app
db.init_app(app)
# 开启crsf保护，然后在表单添加crsf验证
# 可以惰性加载
# crsf.init_app(app)
CSRFProtect(app)
# 邮箱
mail.init_app(app)
# # 阿里大于
# alidayu.init_app(app)
# 注册蓝图
app.register_blueprint(cms_bp)
app.register_blueprint(common_bp)
app.register_blueprint(front_bp)
app.register_blueprint(ueditor_bp)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('front/404.html'), 404

if __name__ == '__main__':
    app.run()
