#coding:utf-8

from flask_script import Manager
from flask_migrate import MigrateCommand,Migrate
from zlbbs import app
from exts import db
# 导入模型
from apps.cms import models as cms_models
from apps.front import models as front_models
from apps import models as apps_models

BoardModel = apps_models.BoardModel
PostModel = apps_models.PostModel
FrontUser = front_models.FrontUser

CMSUser = cms_models.CMSUser
CMSRole = cms_models.CMSRole
CMSPermission = cms_models.CMSPersmission

# 执行命令
# python manage.py db init:初始化
# python manage.py db migrate:执行迁移脚本
# 有可能报AttributeError: 'NoneType' object has no attribute 'encoding'的错误,是因为写数据库连接的时候,utf8写成了utf-8
# python manage.py db upgrade:映射到数据库中

# app = create_app()

# 初始化app
manager = Manager(app)

# 绑定
Migrate(app,db)
# 映射
manager.add_command('db',MigrateCommand)

# 添加cms用户
@manager.option('-u','--username',dest='username')
@manager.option('-p','--password',dest='password')
@manager.option('-e','--email',dest='email')
def create_cms_user(username,password,email):
    user = CMSUser(username=username,password=password,email=email)
    db.session.add(user)
    db.session.commit()
    print("cms用户添加成功")
# 添加数据
# python manage.py create_cms_user -u miku -p 123456 -e 1479852727@qq.com

# 命令，不传参数
# python manage.py create_role
@manager.command
def create_role():
    # 1. 访问者（可以修改个人信息）
    visitor = CMSRole(name='访问者',desc='只能阅读相关数据，不能修改。')
    visitor.permissions = CMSPermission.VISITOR

    # 2. 运营角色（修改个人个人信息，管理帖子，管理评论，管理前台用户）
    operator = CMSRole(name='运营',desc='管理帖子，管理评论,管理前台用户。')
    operator.permissions = CMSPermission.VISITOR|CMSPermission.POSTER|CMSPermission.CMSUSER|CMSPermission.COMMENTER|CMSPermission.FRONTUSER

    # 3. 管理员（拥有绝大部分权限）
    admin = CMSRole(name='管理员',desc='拥有本系统所有权限。')
    admin.permissions = CMSPermission.VISITOR|CMSPermission.POSTER|CMSPermission.CMSUSER|CMSPermission.COMMENTER|CMSPermission.FRONTUSER|CMSPermission.BOARDER

    # 4. 开发者
    developer = CMSRole(name='开发者',desc='开发人员专用角色。')
    developer.permissions = CMSPermission.ALL_PERMISSION

    db.session.add_all([visitor,operator,admin,developer])
    db.session.commit()

# 将用户添加到访问者权限中
# python manage.py add_user_to_role -e 1479852727@qq.com -n 访问者
# 定义添加角色的命令，不然没有访问权限
# 将某用户添加到某角色中
@manager.option('-e','--email',dest='email')
@manager.option('-n','--name',dest='name')
def add_user_to_role(email,name):
    user = CMSUser.query.filter_by(email=email).first()
    if user:
        role = CMSRole.query.filter_by(name=name).first()
        if role:
            role.users.append(user)
            db.session.commit()
            print("用户添加到角色成功")
        else:
            print("没有这个角色:{}".format(name))
    else:
        print("{}邮箱没有这个用户".format(email))

# 在此之前需要映射front的模型到数据库中
# python manage.py db migrate
# python manage.py db upgrade
@manager.option('-t','--telephone',dest='telephone')
@manager.option('-u','--username',dest='username')
@manager.option('-p','--password',dest='password')
def create_front_user(telephone,username,password):
    user = FrontUser(telephone=telephone,username=username,password=password)
    db.session.add(user)
    db.session.commit()


# python manage.py test_permission
@manager.command
def test_permission():
    # 查询用户
    user = CMSUser.query.first()
    if user.is_developer:
        print("拥有访问者权限")
    else:
        print("无访问者权限")

# 添加测试帖子数据
@manager.command
def create_test_post():
    for i in range(1,205):
        title = "标题{}".format(i)
        content = "内容:{}".format(i)
        board = BoardModel.query.first()
        author = FrontUser.query.first()
        post = PostModel(title=title,content=content)
        post.board = board
        post.author = author
        db.session.add(post)
        db.session.commit()
    print("添加帖子测试完成")

if __name__ == "__main__":
    manager.run()


# 添加测试数据
# 访问者
# python manage.py create_cms_user -u 我是访问者 -p 123456 -e visitor@qq.com
# python manage.py add_user_to_role -e visitor@qq.com -n 访问者
# 运营
# python manage.py create_cms_user -u 我是运营者 -p 123456 -e operator@qq.com
# python manage.py add_user_to_role -e operator@qq.com -n 运营
# 管理员
# python manage.py create_cms_user -u 我是管理员 -p 123456 -e admin@qq.com
# python manage.py add_user_to_role -e admin@qq.com -n 管理员
# 开发者
# python manage.py create_cms_user -u 我是开发者 -p 123456 -e developer@qq.com
# python manage.py add_user_to_role -e developer@qq.com -n 开发者


# 前台用户测试数据
# python manage.py create_front_user -t 18892332606 -u angle -p 123456
