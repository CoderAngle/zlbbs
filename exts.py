from flask_sqlalchemy import  SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from flask_mail import Mail
# from utils.alidayu import AlidayuAPI

db = SQLAlchemy()
crsf = CSRFProtect()
mail = Mail()
# alidayu = AlidayuAPI()